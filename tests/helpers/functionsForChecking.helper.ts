import { expect } from "chai";

const schemas = require("../api/specs/data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}

export function checkResponseBodyLength(response, bodyLength: 1) {
    expect(response.body.length, `Response body should have more than ${bodyLength} item`).to.be.greaterThan(
        bodyLength
    );
}

export function checkSchema(
    response,
    schemaName: "schema_allUsers" | "schema_registration" | "schema_user" | "schema_post" | "schema_allPosts"
) {
    expect(response.body).to.be.jsonSchema(schemas[schemaName]);
}
