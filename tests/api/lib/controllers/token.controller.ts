import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class TokenController {
    async refreshToken(accessToken: string, refreshToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Token/refresh`)
            .body({
                accessToken,
                refreshToken,
            })
            .send();
        return response;
    }

    async revokeToken(refreshToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Token/revoke`)
            .body({
                refreshToken,
            })
            .send();
        return response;
    }
}
