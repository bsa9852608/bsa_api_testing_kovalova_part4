import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class CommentController {
    async postComment(authorId: string, postId: string, body: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId,
                postId,
                body,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
