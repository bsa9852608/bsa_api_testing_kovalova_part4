import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostController {
    async getAllPosts() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Posts`).send();
        return response;
    }

    async makePost(previewImage: string, body: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                previewImage,
                body,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async likePost(entityId: string, isLike: boolean, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId,
                isLike,
                // userId: userId,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
