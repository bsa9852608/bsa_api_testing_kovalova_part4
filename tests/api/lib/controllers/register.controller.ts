import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async register(avatar: string, email: string, userName: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                avatar,
                email,
                userName,
                password,
            })
            .send();
        return response;
    }
}
