import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    async login(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email,
                password,
            })
            .send();
        return response;
    }
}
