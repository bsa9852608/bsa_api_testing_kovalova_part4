import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const auth = new AuthController();
const post = new PostController();

describe("Negative tests", () => {
    let userId: string;
    let accessToken: string;
    let postId: string;
    const avatar = "string";
    const email = "olena9989test@gmail.com";
    const userName = "TestQA865222";
    const password = "Test12345";

    before("Register", async () => {
        const registerResponse = await register.register(avatar, email, userName, password);
        const user = registerResponse.body.user;
        userId = user.id;

        const loginResponse = await auth.login(email, password);
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it("Register a new user with empty email", async () => {
        const response = await register.register(avatar, "", userName, password);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);

        const error = response.body;
        expect(error.message).to.be.equal("Validation errors");
        expect(error.errors.length).to.be.greaterThan(0);
    });

    it("Log in using invalid password", async () => {
        const response = await auth.login(email, "Password321");

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);

        const error = response.body;
        expect(error.code).to.be.equal(3);
        expect(error.error).to.be.equal("Invalid username or password.");
    });

    it("Add like to non-existent post", async () => {
        const response = await post.likePost("fakePostid", true, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);

        const error = response.body;
        expect(error.message).to.be.equal("Validation errors");
        expect(error.errors.length).to.be.greaterThan(0);
    });
});
