import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import {
    checkStatusCode,
    checkResponseTime,
    checkResponseBodyLength,
    checkSchema,
} from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

describe("User flow", () => {
    let userId: string;
    let accessToken: string;
    const avatar = "https://www.belta.by/images/storage/news/with_archive/2022/000029_1655798267_509174_big.jpg";
    const email = "olena477478test@gmail.com";
    const userName = "OlenaQA7483";
    const password = "Test123";
    const newUserName = "MyNewName";

    it("1.Register a new user", async () => {
        const response = await register.register(avatar, email, userName, password);
        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_registration");

        const user = response.body.user;
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(userName);
        userId = user.id;
    });

    it("2.Get all users list", async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseBodyLength(response, 1);
        checkSchema(response, "schema_allUsers");

        const userIndex = response.body.findIndex((item) => item.id === userId);
        expect(userIndex).not.eql(-1);

        const user = response.body[userIndex];
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(userName);
    });

    it("3.Log in using valid credentials from the first step", async () => {
        let response = await auth.login(email, password);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

        const user = response.body.user;
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(userName);
        expect(user.id).to.be.equal(userId);

        accessToken = response.body.token.accessToken.token;
    });

    it("4.Get details of the current logged in user", async () => {
        let response = await users.getUserFromToken(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_user");

        const user = response.body;
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(userName);
        expect(user.id).to.be.equal(userId);
    });

    it("5.Update current user name", async () => {
        const userData = {
            id: userId,
            avatar,
            email,
            userName: newUserName,
        };
        let response = await users.updateUser(userData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it("6.Get details of the current logged in user with new name", async () => {
        let response = await users.getUserFromToken(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_user");

        const user = response.body;
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(newUserName);
        expect(user.id).to.be.equal(userId);
    });

    it("7.Get details of current user by its id", async () => {
        let response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_user");

        const user = response.body;
        expect(user.avatar).to.be.equal(avatar);
        expect(user.email).to.be.equal(email);
        expect(user.userName).to.be.equal(newUserName);
        expect(user.id).to.be.equal(userId);
    });

    it("8.Delete current user", async () => {
        let response = await users.deleteUserById(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
});
