import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostController } from "../lib/controllers/posts.controller";
import { CommentController } from "../lib/controllers/comments.controller";
import {
    checkStatusCode,
    checkResponseTime,
    checkResponseBodyLength,
    checkSchema,
} from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();
const auth = new AuthController();
const users = new UsersController();
const post = new PostController();
const comment = new CommentController();

describe("Post functionality", () => {
    let accessToken: string;
    let userId: string;
    let postId: string;
    let authorId: string;
    let commentId: string;
    const avatar = "string";
    const email = "olena64646test@gmail.com";
    const userName = "TestQA88111";
    const password = "Test12345";
    const previewImage = "string";
    const postText = "WOW! It is my first post!";
    const commentText = "Congratulations!";

    before("1.Register", async () => {
        const registerResponse = await register.register(avatar, email, userName, password);
        const user = registerResponse.body.user;
        userId = user.id;

        const loginResponse = await auth.login(email, password);
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it("2.Create new post", async () => {
        const response = await post.makePost(previewImage, postText, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_post");

        const postBody = response.body;
        expect(postBody.previewImage).to.be.equal(previewImage);
        expect(postBody.body).to.be.equal(postText);
        postId = postBody.id;
    });

    it("3.Check if post is added", async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseBodyLength(response, 1);
        checkSchema(response, "schema_allPosts");

        const postIndex = response.body.findIndex((item) => item.id === postId);
        expect(postIndex).not.eql(-1);

        const postBody = response.body[postIndex];
        expect(postBody.previewImage).to.be.equal(previewImage);
        expect(postBody.body).to.be.equal(postText);
    });

    it("4.Add like to the post", async () => {
        const response = await post.likePost(postId, true, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it("5.Check if like is added", async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseBodyLength(response, 1);
        checkSchema(response, "schema_allPosts");

        const postIndex = response.body.findIndex((item) => item.id === postId);
        expect(postIndex).not.eql(-1);

        const postBody = response.body[postIndex];
        const reaction = postBody.reactions.find((reaction) => reaction.user.email === email);
        expect(reaction.isLike).to.be.equal(true);
    });

    it("6.Remove like from the post", async () => {
        const response = await post.likePost(postId, false, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it("7.Check if like is removed", async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseBodyLength(response, 1);
        checkSchema(response, "schema_allPosts");

        const postIndex = response.body.findIndex((item) => item.id === postId);
        expect(postIndex).not.eql(-1);

        const postBody = response.body[postIndex];
        const reactionIndex = postBody.reactions.findIndex((reaction) => reaction.user.email === email);
        expect(reactionIndex).to.be.equal(-1);
    });

    it("8.Add comment to the post", async () => {
        const response = await comment.postComment(authorId, postId, commentText, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

        const commentBody = response.body;
        expect(commentBody.body).to.be.equal(commentText);
        commentId = commentBody.id;
    });

    it("9.Check if comment is added to the post", async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseBodyLength(response, 1);
        checkSchema(response, "schema_allPosts");

        const postIndex = response.body.findIndex((item) => item.id === postId);
        expect(postIndex).not.eql(-1);

        const postBody = response.body[postIndex];
        const comment = postBody.comments.find((comment) => comment.id === commentId);
        expect(comment.body).to.be.equal(commentText);
    });

    after("Delete user", async () => {
        await users.deleteUserById(userId, accessToken);
    });
});
