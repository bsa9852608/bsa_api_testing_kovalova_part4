import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const register = new RegisterController();
const auth = new AuthController();

describe("Test data set for logging in (invalid email)", () => {
    let userId: string;
    const avatar = "string";
    const email = "olena000test@gmail.com";
    const userName = "TestQA000";
    const password = "Test12345";
    let invalidCredentialsDataSet = [
        { email: "another@gmail.com", password: "Test12345" },
        { email: " olena000test@gmail.com", password: "Test12345" },
        { email: "olena000test@gmail.com ", password: "Test12345" },
        { email: "olena000! test@gmail.com", password: "Test12345" },
        { email: "olena000 test@gmail.com", password: "Test12345" },
        { email: "Test12345", password: "Test12345" },
        { email: "admin", password: "Test12345" },
        { email: "     ", password: "Test12345" },
    ];
    before("Register", async () => {
        const registerResponse = await register.register(avatar, email, userName, password);
        const user = registerResponse.body.user;
        userId = user.id;
    });

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`User should not be logged in using invalid email: '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 404);
            checkResponseTime(response, 1000);
        });
    });
});
